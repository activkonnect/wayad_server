# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :
#
# wayad_server
# (c) 2014 ActivKonnect

from django import template
from django.utils.translation import ungettext

register = template.Library()

@register.filter
def secs_to_human(secs):
    if secs < 60:
        return ungettext(
            '{secs} second',
            '{secs} seconds',
            secs
        ).format(secs=secs)
    elif secs < 3600:
        minutes = int(secs / 60)
        return ungettext(
            '{minutes:d} minute',
            '{minutes:d} minutes',
            minutes
        ).format(minutes=minutes)
    else:
        minutes = int((secs % 3600) / 60)
        hours = int(secs / 3600)
        return ungettext(
            '{hours}:{minutes:02} hour',
            '{hours}:{minutes:02} hours',
            hours
        ).format(minutes=minutes, hours=hours)
