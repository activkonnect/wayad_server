# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :
#
# wayad_server
# (c) 2014 ActivKonnect

from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from rest_framework import routers
from wayad_server.apps.register.views import UserViewSet
from wayad_server.apps.timelog.views import EntryViewSet, ReferenceViewSet


router = routers.DefaultRouter()
router.register('entries', EntryViewSet)
router.register('users', UserViewSet)
router.register('references', ReferenceViewSet)


urlpatterns = patterns(
    '',

    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^accounts/', include('wayad_server.apps.register.urls', namespace='register')),
    url(r'^api/v1/', include(router.urls)),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
