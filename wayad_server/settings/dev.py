# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :
#
# wayad_server
# (c) 2014 ActivKonnect

from .common import *

DEBUG = True
TEMPLATE_DEBUG = True

COMPRESS_ENABLED = True
