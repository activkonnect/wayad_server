# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :
#
# wayad_server
# (c) 2014 ActivKonnect

from os import getenv

import dj_database_url

from .utils import *

DEBUG = False
TEMPLATE_DEBUG = False

ALLOWED_HOSTS = []
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')

SITE_ID = 1

INSTALLED_APPS = (
    # Base Django apps
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    # Helper apps
    'raven.contrib.django.raven_compat',
    'djangobower',
    'pipeline',
    'sorl.thumbnail',
    'crispy_forms',
    'metron',
    'rest_framework',
    'rest_framework.authtoken',

    # Our apps
    'wayad_server.apps.register',
    'wayad_server.apps.timelog',
    'wayad_server.lib.wayad_tags',

    # Django Allauth
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    # General processors
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',

    # Django Allauth specific
    'allauth.account.context_processors.account',
    'allauth.socialaccount.context_processors.socialaccount',
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAdminUser',
    ),
    'PAGINATE_BY': 30,
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
}

ROOT_URLCONF = 'wayad_server.urls'

WSGI_APPLICATION = 'wayad_server.wsgi.application'


# Database
DATABASES = {
    'default': dj_database_url.config(),
}

# Internationalization
LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Paris'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
STATIC_URL = '/static/'

STATIC_ROOT = DJANGO_ROOT.child('assets')

STATICFILES_DIRS = (
    DJANGO_ROOT.child('static'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'djangobower.finders.BowerFinder',
    'pipeline.finders.PipelineFinder',
)

STATICFILES_STORAGE = 'pipeline.storage.PipelineCachedStorage'

# Templates
TEMPLATE_DIRS = (
    DJANGO_ROOT.child('templates'),
)

# Get SECRET_KEY from env
SECRET_KEY = getenv('SECRET_KEY')

# Email sending
DEFAULT_FROM_EMAIL = 'EMAIL-SENDER@EXAMPLE.COM'
SERVER_EMAIL = 'no-reply@aksrv.net'

# Analytics
METRON_SETTINGS = {
    "google": {
        1: getenv('GOOGLE_ANALYTICS_UID', ''),
    }
}

# Forms
CRISPY_TEMPLATE_PACK = 'bootstrap3'

# Jira
JIRA_OPTIONS = {
    'server': getenv('JIRA_SERVER_URL'),
}

JIRA_ISSUES_QUERY = 'resolution IS EMPTY AND status != Done'

# Other common config files
from .bower import *
from .pipeline import *
from .auth import *
