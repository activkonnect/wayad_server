# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :
#
# wayad_server
# (c) 2014 ActivKonnect

PIPELINE_COMPILERS = (
    'pylesswrap.pipeline_compiler.PyLessWrapCompiler',
)

PIPELINE_CSS = {
    'bootstrap': {
        'source_filenames': (
            'wayad_server/bootstrap/bootstrap.less',
        ),
        'output_filename': 'css/bootstrap.css',
    },

    'style': {
        'source_filenames': (
            'wayad_server/less/layout.less',
        ),
        'output_filename': 'css/style.css',
    },
}
