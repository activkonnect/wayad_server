# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :
#
# wayad_server
# (c) 2014 ActivKonnect

from .models import User
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name')
