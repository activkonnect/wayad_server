# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :
#
# wayad_server
# (c) 2014 ActivKonnect

from datetime import timedelta
from django.shortcuts import render
from django.utils import timezone
from natsort import natsorted
from rest_framework import viewsets
from .models import User
from .serializers import UserSerializer
from wayad_server.apps.timelog.models import Entry


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


def profile(request):
    return render(request, 'wayad/profile.html', {
        'endpoint_url': request.build_absolute_uri('/api/v1/')
    })


def report(request):
    d1 = timezone.now()
    d2 = d1 - timedelta(days=7)
    tasks = {str(x): y
             for x, y in Entry.count_task_time(Entry.put_in_slot_between(d2, d1)).items()}
    total = sum(x[1] for x in tasks.items())

    return render(request, 'wayad/report.html', {
        'tasks': natsorted(tasks.items(), signed=False),
        'total': total,
    })
