# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :
#
# wayad_server
# (c) 2014 ActivKonnect

from django.conf.urls import patterns, url

urlpatterns = patterns(
    'wayad_server.apps.register.views',

    url(r'^profile/$', 'profile', name='profile'),
    url(r'^report/$', 'report', name='report'),
)
