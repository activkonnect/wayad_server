# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :
#
# wayad_server
# (c) 2014 ActivKonnect

from .models import Entry
from rest_framework import serializers
from wayad_server.apps.timelog.models import Reference


class EntrySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Entry
        fields = ('id', 'user', 'begin', 'date', 'reference', 'message')
        read_only_fields = ('date', 'user')


class ReferenceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Reference
        fields = ('id', 'key', 'message')


