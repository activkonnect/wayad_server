# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :
#
# wayad_server
# (c) 2014 ActivKonnect

from collections import defaultdict
from django.db import models
from django.conf import settings
from django.db.models import Q
from django.utils import timezone
from datetime import timedelta
from jira.client import JIRA


class Reference(models.Model):
    key = models.CharField(max_length=100, unique=True, null=True)
    message = models.CharField(max_length=1000)
    active = models.BooleanField(default=False)

    def __str__(self):
        if self.key is not None:
            return '[{}] {}'.format(self.key, self.message)

        return self.message


class Entry(models.Model):
    SLOT_DURATION = 1800

    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    begin = models.DateTimeField(null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True)
    message = models.CharField(max_length=140)
    reference = models.ForeignKey(Reference, null=True, blank=True, related_name='entries')
    duration = models.IntegerField(null=True, blank=True, help_text='Entry duration in seconds')
    reported = models.BooleanField(default=False, help_text='Reported to upstream reference')

    @property
    def estimate_begin(self):
        begin = self.begin if self.begin is not None else self.date
        estimate = timezone.datetime.utcfromtimestamp(
            begin.timestamp() - (begin.timestamp() % Entry.SLOT_DURATION)
        )
        return estimate.replace(tzinfo=timezone.utc)

    @staticmethod
    def calculate_missing():
        slots = Entry.put_in_slot_uncalculated()
        for slot_id, entries in slots.items():
            duration = Entry.SLOT_DURATION / len(entries)
            for entry in entries:
                entry.duration = duration if entry.duration is None else entry.duration + duration
                entry.save()

    @staticmethod
    def put_in_slot(entries):
        slots = defaultdict(lambda: [])

        for entry in entries:
            start = int(entry.begin_auto.timestamp() / Entry.SLOT_DURATION)
            stop = int(entry.date.timestamp() / Entry.SLOT_DURATION) + 1

            for slot in range(start, stop):
                slots[slot].append(entry)

        return slots

    @staticmethod
    def put_in_slot_between(start, stop):
        entries = Entry.objects.extra(
            select={
                'begin_auto': 'COALESCE(begin, date)',
            },
            where=['%s < COALESCE(begin, date)', 'date <= %s'],
            params=[start, stop],
        )

        return Entry.put_in_slot(entries)

    @staticmethod
    def put_in_slot_uncalculated():
        max_date = timezone.now() - timedelta(seconds=Entry.SLOT_DURATION) - timedelta(minutes=5)
        entries = Entry.objects.filter(date__lt=max_date).filter(duration__isnull=True).extra(
            select={
                'begin_auto': 'COALESCE(begin, date)',
            },
        )
        return Entry.put_in_slot(entries)

    @staticmethod
    def count_task_time(slots):
        tasks = defaultdict(lambda: 0)

        for slot_id, slot_tasks in slots.items():
            for task in slot_tasks:
                if task.reference is not None:
                    key = task.reference
                else:
                    key = task.message
                tasks[key] += Entry.SLOT_DURATION / len(slot_tasks)

        return dict(tasks)

    def save(self, *args, **kwargs):
        if self.reference is None:
            self.reference = Reference.objects.get_or_create(
                message=self.message,
                active=True,
            )[0]

        super(Entry, self).save(*args, **kwargs)


class JiraAccount(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='jira_account')
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=100)


class JiraIssue(models.Model):
    key = models.CharField(max_length=100, unique=True)
    message = models.CharField(max_length=1000)
    active = models.BooleanField(default=False)
    owners = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='jira_issues')
    reference = models.OneToOneField(Reference, related_name='jira_issue')

    def save(self, *args, **kwargs):
        if self.reference_id is None:
            self.reference = Reference.objects.create(
                key=self.key,
                message=self.message,
                active=self.active,
            )
        else:
            has_diff = False

            for field in ('key', 'message', 'active'):
                if getattr(self, field) != getattr(self.reference, field):
                    setattr(self.reference, field, getattr(self, field))
                    has_diff = True

            if has_diff:
                self.reference.save()

        super(JiraIssue, self).save(*args, **kwargs)

    @staticmethod
    def get_jira(user):
        jira = JIRA(
            options=settings.JIRA_OPTIONS,
            basic_auth=(user.jira_account.username, user.jira_account.password),
        )
        return jira

    @staticmethod
    def upload_timesheet(user):
        jira = JiraIssue.get_jira(user)

        for entry in (Entry.objects
                      .filter(user=user)
                      .filter(reported=False)
                      .filter(duration__isnull=False)
                      .filter(reference__jira_issue__isnull=False)
                      .select_related('reference')):
            jira.add_worklog(
                entry.reference.key,
                timeSpentSeconds=entry.duration,
                started=entry.estimate_begin,
            )
            entry.reported = True
            entry.save()

    @staticmethod
    def sync_for_user(user):
        try:
            jira = JiraIssue.get_jira(user)

            issues = list(search_all_issues(jira, settings.JIRA_ISSUES_QUERY))
            expected_ids = set(issue.key for issue in issues)
            expected_own = set()

            for issue in user.jira_issues.filter(~Q(key__in=expected_ids) | Q(active=True)):
                issue.active = False
                issue.save()

            for issue in issues:
                db_issue, created = JiraIssue.objects.get_or_create(
                    key=issue.key,
                    defaults={
                        'message': issue.fields.summary,
                        'active': True,
                    }
                )

                if not created:
                    diff = False

                    if db_issue.message != issue.fields.summary:
                        db_issue.message = issue.fields.summary
                        diff = True

                    if not db_issue.active:
                        db_issue.active = True
                        diff = True

                    if diff:
                        db_issue.save()

                expected_own.add(db_issue)

            actual_own = set(user.jira_issues.filter(active=True))

            for issue in expected_own - actual_own:
                user.jira_issues.add(issue)
        except JiraAccount.DoesNotExist:
            pass


def search_all_issues(jira, query):
    found = 0
    total = 1

    while found < total:
        results = jira.search_issues(query, startAt=found, fields=('summary', 'id'))
        total = results.total
        for issue in results:
            yield issue
            found += 1
