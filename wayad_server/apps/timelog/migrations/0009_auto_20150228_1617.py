# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('timelog', '0008_auto_20150228_1547'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='duration',
            field=models.IntegerField(null=True, blank=True, help_text='Entry duration in seconds'),
        ),
        migrations.AddField(
            model_name='entry',
            name='reported',
            field=models.BooleanField(default=False, help_text='Reported to upstream reference'),
        ),
    ]
