# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('timelog', '0006_auto_20150128_2001'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jiraissue',
            name='reference',
            field=models.ForeignKey(to='timelog.Reference', related_name='jira_issues'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='reference',
            name='key',
            field=models.CharField(unique=True, null=True, max_length=100),
            preserve_default=True,
        ),
    ]
