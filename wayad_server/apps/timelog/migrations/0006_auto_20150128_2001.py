# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('timelog', '0005_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='reference',
            field=models.ForeignKey(
                to='timelog.Reference',
                related_name='entries',
                null=True,
                blank=True
            ),
            preserve_default=True,
        ),
    ]
