# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('timelog', '0007_auto_20150204_2055'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jiraissue',
            name='reference',
            field=models.OneToOneField(related_name='jira_issue', to='timelog.Reference'),
        ),
    ]
