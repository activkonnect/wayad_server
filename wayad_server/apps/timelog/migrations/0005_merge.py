# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('timelog', '0003_entry_begin'),
        ('timelog', '0004_auto_20150115_2336'),
    ]

    operations = [
    ]
