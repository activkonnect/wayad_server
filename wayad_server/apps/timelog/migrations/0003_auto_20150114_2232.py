# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('timelog', '0002_auto_20150112_2244'),
    ]

    operations = [
        migrations.CreateModel(
            name='JiraAccount',
            fields=[
                ('id', models.AutoField(
                    primary_key=True,
                    verbose_name='ID',
                    auto_created=True,
                    serialize=False
                )),
                ('username', models.CharField(max_length=100)),
                ('password', models.CharField(max_length=100)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='JiraIssue',
            fields=[
                ('id', models.AutoField(
                    primary_key=True,
                    verbose_name='ID',
                    auto_created=True,
                    serialize=False
                )),
                ('key', models.CharField(max_length=100)),
                ('message', models.CharField(max_length=1000)),
                ('active', models.BooleanField(default=False)),
                ('owners', models.ManyToManyField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Reference',
            fields=[
                ('id', models.AutoField(
                    primary_key=True,
                    verbose_name='ID',
                    auto_created=True,
                    serialize=False
                )),
                ('key', models.CharField(max_length=100)),
                ('message', models.CharField(max_length=1000)),
                ('active', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='jiraissue',
            name='reference',
            field=models.ForeignKey(to='timelog.Reference'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='entry',
            name='reference',
            field=models.ForeignKey(null=True, to='timelog.Reference', blank=True),
            preserve_default=True,
        ),
    ]
