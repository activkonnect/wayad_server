# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('timelog', '0003_auto_20150114_2232'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jiraaccount',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL, related_name='jira_account'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='jiraissue',
            name='key',
            field=models.CharField(max_length=100, unique=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='jiraissue',
            name='owners',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, related_name='jira_issues'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='reference',
            name='key',
            field=models.CharField(max_length=100, unique=True),
            preserve_default=True,
        ),
    ]
