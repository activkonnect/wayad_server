# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('timelog', '0002_auto_20150112_2244'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='begin',
            field=models.DateTimeField(blank=True, null=True),
            preserve_default=True,
        ),
    ]
