# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :
#
# wayad_server
# (c) 2014 ActivKonnect

from django.db.models import Q, Max
from django.db.models.expressions import Func, F
from rest_framework import viewsets
from .models import Entry
from .serializers import EntrySerializer
from wayad_server.apps.timelog.models import Reference
from wayad_server.apps.timelog.serializers import ReferenceSerializer


class EntryViewSet(viewsets.ModelViewSet):
    queryset = Entry.objects.all()
    serializer_class = EntrySerializer

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ReferenceViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Reference.objects.filter(active=True)\
        .annotate(date=Max(Func(
            F('entries__date'),
            function='COALESCE',
            template='%(function)s(%(expressions)s, date \'1970-01-01\')'
        )))\
        .order_by('-date', '-key')
    serializer_class = ReferenceSerializer

    def get_queryset(self):
        qs = self.queryset.filter(Q(jira_issue__owners=self.request.user) |
                                  Q(entries__user=self.request.user))

        fltr = self.request.GET.get('filter', None)

        if fltr is not None:
            qs = qs.filter(Q(key__istartswith=fltr) | Q(message__istartswith=fltr))

        return qs
