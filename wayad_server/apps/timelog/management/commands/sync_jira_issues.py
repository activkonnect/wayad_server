# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :
#
# wayad_server
# (c) 2014 ActivKonnect

import traceback
from django.core.management.base import BaseCommand
from django.utils.translation import ungettext
from wayad_server.apps.timelog.models import JiraAccount, JiraIssue


class Command(BaseCommand):
    help = 'Syncs JIRA issues for all users'

    def handle(self, *args, **options):
        q = JiraAccount.objects.all().select_related('user')
        n = q.count()
        fail_count = 0

        self.stdout.write(self.style.MIGRATE_HEADING(ungettext(
            '{} account to check',
            '{} accounts to check',
            n,
        ).format(n)))

        for account in q:
            self.stdout.write('  {}... '.format(account.user.get_full_name()), ending='')
            self.stdout.flush()

            # noinspection PyBroadException
            try:
                JiraIssue.sync_for_user(account.user)
                self.stdout.write(self.style.MIGRATE_SUCCESS('ok'))
            except:
                self.stdout.write(self.style.ERROR('fail'))
                self.stderr.write('')
                self.stderr.write('Exception:')
                self.stderr.write(traceback.format_exc())
                self.stderr.write('')
                fail_count += 1

        if fail_count == 0:
            self.stdout.write(self.style.MIGRATE_SUCCESS('Done'))
        else:
            self.stdout.write(self.style.MIGRATE_SUCCESS('Done')
                              + self.style.NOTICE(' (with errors)'))
            exit(1)
