# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :
#
# wayad_server
# (c) 2015 ActivKonnect

import traceback
from django.core.management.base import BaseCommand
from django.utils.translation import ungettext
from wayad_server.apps.timelog.models import Entry, JiraAccount, JiraIssue


class Command(BaseCommand):
    help = 'Sync JIRA timesheets for all pending tasks'

    def handle(self, *args, **options):
        self.stdout.write(self.style.MIGRATE_HEADING('Calculating durations...'), ending='')
        Entry.calculate_missing()
        self.stdout.write(self.style.MIGRATE_SUCCESS(' done'), ending='\n\n')

        q = JiraAccount.objects.all().select_related('user')
        n = q.count()
        fail_count = 0

        self.stdout.write(self.style.MIGRATE_HEADING(ungettext(
            '{} account to sync',
            '{} accounts to sync',
            n,
        ).format(n)))

        for account in q:
            self.stdout.write('  {}... '.format(account.user.get_full_name()), ending='')
            self.stdout.flush()

            # noinspection PyBroadException
            try:
                JiraIssue.upload_timesheet(account.user)
                self.stdout.write(self.style.MIGRATE_SUCCESS('ok'))
            except:
                self.stdout.write(self.style.ERROR('fail'))
                self.stderr.write('')
                self.stderr.write('Exception:')
                self.stderr.write(traceback.format_exc())
                self.stderr.write('')
                fail_count += 1

        if fail_count == 0:
            self.stdout.write(self.style.MIGRATE_SUCCESS('Done'))
        else:
            self.stdout.write(self.style.MIGRATE_SUCCESS('Done')
                              + self.style.NOTICE(' (with errors)'))
            exit(1)
